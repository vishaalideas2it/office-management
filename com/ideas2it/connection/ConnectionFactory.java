package com.ideas2it.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;

/**
 * <p>
 * A factory which creates a single connection for accessing the data objects.
 * </p>
 */
public class ConnectionFactory {
    private static ConnectionFactory factory = new ConnectionFactory();
    private static Connection connection = null;

    /**
     * <p>
     * Setting the Driver for the database vendor. ie) MySQL Driver.
     * </p>
     */
    private ConnectionFactory() {
    }

    /** 
     * <p>
     * Creating a database connection using the driver by giving the database
     * Credentials and URL for the Database server API.
     * </p>
     *
     * @return a connection which has created.
     *
     * @exception ApplicationException when MySQL driver class is not found and 
     *            unable to establish connection with the MySQL server.
     */
    private Connection createConnection() throws ApplicationException {
        try {
            Class.forName(Constants.JDBC_DRIVER);
            return DriverManager.getConnection(Constants.DB_MYSQL_URL, 
                                   Constants.MYSQL_USER, Constants.MYSQL_PASS);
        } catch (SQLException e) {
            ApplicationLogger.error(Constants.DEV_ERR_CONN , e);
            throw new ApplicationException(e);
        } catch (ClassNotFoundException e) {
            ApplicationLogger.error(Constants.ERR_CLASS_MYSQL
                                       + Constants.JDBC_DRIVER, e);
            throw new ApplicationException(e);
        }
    }

    /** 
     * <p>
     * Getting connection from the factory. if the existing connection is closed
     * then create a new connection.
     * </p>
     *
     * @return connection which is open for accessing the database.
     *
     * @exception ApplicationException when unable to establish connection with 
     *            the MySQL server.
     */
    public static Connection getConnection() throws ApplicationException {
        try {
            if (null == connection || connection.isClosed()) {
                connection = factory.createConnection();
            }
        } catch (SQLException e) {
            ApplicationLogger.error(Constants.ERR_SINGLE_CONN, e);
            throw new ApplicationException(e);
        }  catch (ApplicationException e) {
            ApplicationLogger.error(Constants.ERR_SINGLE_CONN, e);
            throw new ApplicationException(e);
        } 
        return connection;
    }

    /**
     * <p>
     * Closing connection as it is not in use.
     * </p>
     */
    public static void close(Connection connection) {
        if (null != connection){
            try {
                connection.close();
            } catch (SQLException e) {
               e.printStackTrace();
            }
        }
    }

    /**
     * <p>
     * Closing statement as it is not in use.
     * </p>
     */
    public static void close(Statement statement) {
        if (null != statement){
            try {
                statement.close();
            } catch (SQLException e) {
               e.printStackTrace();
            }
        }
    }
}
