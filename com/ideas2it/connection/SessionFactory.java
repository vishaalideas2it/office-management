package com.ideas2it.connection;

import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException; 
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.Session; 
import org.hibernate.Transaction;

import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * A factory which creates sessios for accessing the data objects such as
 * Employee, Project,Address and Client.
 * </p>
 */
public class SessionFactory { 

    private static SessionFactory factory = new SessionFactory();
    public static org.hibernate.SessionFactory sessionFactory = null;

    private SessionFactory() {}

    /**
     * <p>
     * Creating a singleton Session factory which can be used to create session.
     *
     * </p>
     * @return sessionFactory - which has been created based on the
     *                          configuration given in the file.
     */
    private org.hibernate.SessionFactory createSessionFactory() throws
            ApplicationException {
        try {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml"); 
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().
                                  applySettings(configuration.getProperties()).
                                      buildServiceRegistry();
            return configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable cause) {
            ApplicationLogger.error("Error caused while creating Session"
                                       + "Factory", cause);
            throw new ApplicationException(cause);
        }
    }

    /**
     * <p>
     * Creating a Session which can be used to access the data object.
     * </p>
     *
     * @return session - which is created by the session factory.
     */
    public static Session getSession() {
        if (null == sessionFactory || sessionFactory.isClosed()) {
            sessionFactory = factory.createSessionFactory();
        }
        return sessionFactory.openSession();
    }
 
    /**
     * <p>
     * Closing a Session factory as the application is about to close.
     * </p>
     *
     */
    public static void closeSessionFactory() {
        if (null != sessionFactory || sessionFactory.isClosed()) {
            sessionFactory.close();
        }
    }
}
