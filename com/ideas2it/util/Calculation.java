package com.ideas2it.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import com.ideas2it.common.Constants;

/** 
 * <p>
 * Calulating some value by the given value based on some buisness logic.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class Calculation {

    /**
     * <p>
     * Calculating Age by the given date.
     * </p>
     * 
     * @param date - through which age is calculated.
     * 
     * @return age which has been calculated by the given date.
     */
    public static int calculateAge(String date) {
        int age = 0;
        try {
            Calendar today = Calendar.getInstance();
    	    Calendar dob = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
	        dob.setTime(sdf.parse(date));
	        int curYear = today.get(Calendar.YEAR);
    	    int dobYear = dob.get(Calendar.YEAR);
	        age = curYear - dobYear;
	        int curMonth = today.get(Calendar.MONTH);
    	    int dobMonth = dob.get(Calendar.MONTH);
	        if (dobMonth > curMonth) { 
	            age--;
    	    } else if (dobMonth == curMonth) { 
	            int curDay = today.get(Calendar.DAY_OF_MONTH);
	            int dobDay = dob.get(Calendar.DAY_OF_MONTH);
	            if (dobDay > curDay) {
    	            age--;
	            }
	        }
        } catch (Exception e) {
            e.printStackTrace();
        }	 
	    return age;
    }
}
 
