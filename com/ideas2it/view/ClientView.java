package com.ideas2it.view;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class ClientView {

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public void showClientCRUDMenu() {
        UI.display(Constants.CLIENT_CRUD_MENU);
    }

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }

    /**
     *<p>
     * Getting client details from user.
     *</p>
     * @return client which has client details entered by user.
     */
    public Client getClientDetailsFromUser() {
        return new Client(
                   UI.getUserInput(Constants.CLIENT_NAME),
                   UI.getUserInput(Constants.EMAIL));
    }

    /** 
     *<p>
     * Displaying details of the given client to the user.
     *</p>
     * @param client - which is to be displayed to the user.
     */
    public void displayClients(List<Client> clients) {
        for (Client client : clients)
            UI.display(String.format(Constants.DISPLAY_CLIENT,
                                        client.getId(), client.getName(),
                                        client.getEmailId()));
    }

    /**
     *<p>
     * Displaying details of the given client to the user.
     *</p>
     * @param client - which is to be displayed to the user.
     */
    public void displayClient(Client client) {
        if ( null == client) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants
                                        .CLIENT));
        } else {
            UI.display(String.format(Constants.DISPLAY_CLIENT,
                                        client.getId(), client.getName(),
                                        client.getEmailId()));
            UI.display(Constants.PROJECT_UNDER_CLIENT);
            new ProjectView().displayProjects(client.getProjects());
        }
    }

    /**
     *<p>
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     *</p>
     */
    public void showCorrectFormat(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            UI.display(Constants.MSG_NAME_FORMAT);
        } else if (attribute.equals(Constants.EMAIL)) {
            UI.display(Constants.MSG_EMAIL_FORMAT);
        } else if (attribute.equals(Constants.DOB)) {
            UI.display(Constants.MSG_DATE_FORMAT);
        } else if (attribute.equals(Constants.SALARY)) {
            UI.display(Constants.MSG_SALARY_FORMAT);
        }
    }

   /**
     *<p>
     * Getting valid client details from user.
     *</p>
     * @return client which has valid client details entered by user.
     */
    public Client getValidClientDetailsFromUser(Client client) {
        if (client.getName().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            client.setName(UI.getUserInput(Constants.NAME));
        }
        if (client.getEmailId().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.EMAIL);
            client.setEmailId(UI.getUserInput(Constants.EMAIL));
        }
        return client;
    }

    /**
     *<p>
     * Showing user a set of options to choose for updation.
     *</p>
     */
    public void showClientUpdateMenu() {
        UI.display(Constants.CLIENT_UPD_MENU);
    }

    /**
     *<p>
     * Showing user by the given message.
     *</p>
     */
    public void showMessage(String message) {
        UI.display(message);
    }
}
