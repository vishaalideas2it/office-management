package com.ideas2it.view;

import java.util.Set;
import java.util.HashSet;
import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.EmployeeController;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class EmployeeView {

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public void showEmployeeCRUDMenu() {
        UI.display(Constants.EMP_CRUD_MENU);
    }

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public static void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }


    /**
     *<p>
     * Getting employee details from user.
     *</p>
     * @return employee which has employee details entered by user.           
     */
    public Employee getEmployeeDetailsFromUser() {
        return (new Employee(0,
                    UI.getUserInput(Constants.NAME),
                    UI.getUserInput(Constants.EMAIL),
                    UI.getUserInput(Constants.DOB), 0,
                    UI.getUserInputNumber(Constants.SALARY)));
    }

    /**
     *<p>
     * Get valid employee detail from user
     *</p>
     */
    public Set<Employee> getValidEmployeesFromUser() {
       Set<Employee> employees = new HashSet<Employee>();
       EmployeeController employeeController = new EmployeeController();
       Employee employee = null;
       do {
           employee = employeeController.validateEmployee
                                                (getEmployeeDetailsFromUser());
           while (employee.getId() < 0) {
               employee.setId(0);
               employee = getValidEmployeeDetailsFromUser(employee);
               employee = employeeController.validateEmployee(employee);
           }
           employees.add(employee);
       } while(UI.getConfirmationFromUser(Constants.WANT_ADD));
       return employees;
    }

   /**
     *<p>
     * Getting valid employee details from user.
     *</p>
     * @return employee which has valid employee details entered by user.
     */
    public Employee getValidEmployeeDetailsFromUser(Employee employee) {
        if (employee.getName().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            employee.setName(UI.getUserInput(Constants.NAME));
        }
        if (employee.getEmailId().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.EMAIL);
            employee.setEmailId(UI.getUserInput(Constants.EMAIL));
        }
        if (employee.getDateOfBirth().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.DOB);
            employee.setDateOfBirth(UI.getUserInput(Constants.DOB));
        }
        if (employee.getSalary() == 0) {
            showCorrectFormat(Constants.SALARY);
            employee.setSalary(UI.getUserInputNumber(Constants.SALARY));
        }
        return employee;
    }

    /**
     *<p>
     * Displaying details of the given employee to the user.
     * </p>
     * @param employee - which has details to be displayed to the user.
     */
    public void displayEmployee(Employee employee) {
        if ( 0 == employee.getId()) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants.
                          EMPLOYEE));
        } else {
            UI.display(String.format(Constants.DISPLAY_EMPLOYEE, employee.
                           getId(), employee.getName(), employee.getEmailId(),
                           employee.getDateOfBirth(), employee.getSalary(), 
                           employee.getAge(), employee.getProject().getId()));
        }
    }

    /**
     *<p>
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     *</p>
     */
    public void showCorrectFormat(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            UI.display(Constants.MSG_NAME_FORMAT);
        } else if (attribute.equals(Constants.EMAIL)) {
            UI.display(Constants.MSG_EMAIL_FORMAT);
        } else if (attribute.equals(Constants.DOB)) {
            UI.display(Constants.MSG_DATE_FORMAT);
        } else if (attribute.equals(Constants.SALARY)) {
            UI.display(Constants.MSG_SALARY_FORMAT);
        }
    }

    /**
     *<p>
     * Showing user a set of options to choose for updation.
     *</p>
     */
    public void showEmployeeUpdateMenu() {
        UI.display(Constants.EMP_UPD_MENU);
    }

    /**
     *<p>
     * Showing user by the given message.
     *</p>
     */
    public void showMessage(String message) {
        UI.display(message);
    }

    /**
     *<p>
     * Showing user by the given message.
     *</p>
     */
    public void showMessage(List<String> messages) {
        int count = 1;
        for(String message: messages)
        UI.display(count++ + ". "+message);
    }
}
