package com.ideas2it.view;

import java.util.Scanner;

import com.ideas2it.common.Constants;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class UI {

    /**
     * Getting a specified attribute from user.
     *
     * @return attribute value which user has entered.
     */
    public static String getUserInput(String attribute) {
        System.out.print(String.format(Constants.MSG_ENTER, attribute));
        return (new Scanner(System.in).nextLine());
    }

    /**
     * Getting a specified attribute from user as a Number.
     *
     * @return attribute value(Number) which user is entered by the user.
     */
    public static int getUserInputNumber(String attribute) {
        System.out.print(String.format(Constants.MSG_ENTER, attribute));
        String number = null;
        try {
            number = new Scanner(System.in).nextLine();
            return (Integer.parseInt(number));
        } catch (NumberFormatException e) {
            ApplicationLogger.error("Invalid Number:"+number, e);
            return getUserInputNumber(attribute);
        }
    }

    /**
     * Getting Confirmation from user .
     *
     * @return true if user presses y for further process else return false.
     */
    public static boolean getConfirmationFromUser(String msg) {
        System.out.println(msg);
        System.out.println(Constants.MSG_CONFIRM);
        char pressed = (new Scanner(System.in).nextLine()).charAt(0);
        return (Constants.YES_LOWER == pressed || Constants.YES == pressed);
    }

    /**
     * Displaying message to the user .
     *
     * @return true if user presses y for further process else return false.
     */
    public static void display(String message) {
        System.out.println(message);
    }
}
