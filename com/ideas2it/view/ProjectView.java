package com.ideas2it.view;

import java.util.List;
import java.util.Set;
import java.util.HashSet;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.ProjectController;
import com.ideas2it.model.Client;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class ProjectView {

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public void showProjectCRUDMenu() {
        UI.display(Constants.PROJECT_CRUD_MENU);
    }

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }

    /**
     *<p>
     * Getting project details from user.
     *</p>
     * @return project which has project details entered by user.
     */
    public Project getProjectDetailsFromUser() {
        return (new Project(0,
                   UI.getUserInput(Constants.PROJECT_NAME)));
    }

    /**
     *<p>
     * Get valid project detail from user
     *</p>
     */
    public Set<Project> getValidProjectsFromUser() {
       Set<Project> projects = new HashSet<Project>();
       ProjectController projectController = new ProjectController();
       Project project = null;
       do {
           project = projectController.validateProject
                                                (getProjectDetailsFromUser());
           while (project.getId() < 0) {
               project.setId(0);
               project = getValidProjectDetailsFromUser(project);
               project = projectController.validateProject(project);
           }
           projects.add(project);
       } while(UI.getConfirmationFromUser(Contants.WANT_ADD));
       return projects;
    }

    /**
     *<p>
     * Getting project details from user.
     *</p>
     * @return project which has project details entered by user.
     */
    public Project getValidProjectDetailsFromUser(Project project) {
        if (project.getName().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            project.setName(UI.getUserInput(Constants.NAME));
        }
        if (project.getClient().getEmailId().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.EMAIL);
            project.getClient().setEmailId(UI.getUserInput(Constants.EMAIL));
        }
        return project;
    }

    /**
     *<p>
     * Displaying details of the given project to the user.
     *</p> 
     * @param project - which is to be displayed to the user.
     */
    public void displayProject(Project project) {
        if ( null == project) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants
                              .PROJECT));
        } else {
            UI.display(String.format(Constants.DISPLAY_PROJECT,
                                        project.getId(), project.getName(),
                                        project.getClient().getId()));
        }
    }

    /**
     *<p>
     * Displaying a set of projects.
     *</p>
     * @param projects - which are to be displayed to the user.
     */
    public void displayProjects(Set<Project> projects) {
        for (Project project : projects) {
            displayProject(project);
        }
    }

    /**
     *<p>
     * Displaying a list of projects.
     *</p>
     * @param projects - which are to be displayed to the user.
     */
    public void displayProjects(List<Project> projects) {
        for (Project project : projects) {
            displayProject(project);
        }
    }


    /**
     *<p>
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     *</p>
     */
    public void showCorrectFormat(String attribute) {
        if (attribute.equals(Constants.NAME)) {
            UI.display(Constants.MSG_NAME_FORMAT);
        } else if (attribute.equals(Constants.EMAIL)) {
            UI.display(Constants.MSG_EMAIL_FORMAT);
        } else if (attribute.equals(Constants.DOB)) {
            UI.display(Constants.MSG_DATE_FORMAT);
        } else if (attribute.equals(Constants.SALARY)) {
            UI.display(Constants.MSG_SALARY_FORMAT);
        }
    }


    /**
     *<p>
     * Showing user a set of options to choose for updation.
     *</p>
     */
    public void showProjectUpdateMenu() {
        UI.display(Constants.PROJECT_UPD_MENU);
    }

    /**
     *<p>
     * Showing user by the given message.
     *</p>
     */
    public void showMessage(String message) {
        UI.display(message);
    }

    /**
     *<p>
     * Showing user by the given message.
     *</p>
     */
    public void showMessage(List<String> messages) {
        int count = 1;
        for(String message: messages)
        UI.display(count++ + ". "+message);
    }
}
