package com.ideas2it.view;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.AddressController;
import com.ideas2it.model.Address;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a User interface which interacts with the user.
 * Input and output can be controlled through this using console.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class AddressView {

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public void showAddressCRUDMenu() {
        UI.display(Constants.ADD_CRUD_MENU);
    }

    /**
     *<p>
     * Showing user a set of options to choose.
     *</p>
     */
    public static void showMainMenu() {
        UI.display(Constants.MAIN_MENU);
    }



    /**
     *<p>
     * Getting address details from user.
     *</p>
     * @return address which has address details entered by user.
     */
    public Address getAddressDetailsFromUser() {
        return new Address(0, UI.getUserInput(Constants.HOLDER_NAME),
                    UI.getUserInput(Constants.ADDRESS_ONE),
                    UI.getUserInput(Constants.ADDRESS_TWO),
                    UI.getUserInput(Constants.CITY),
                    UI.getUserInput(Constants.ZIP),
                    UI.getUserInput(Constants.STATE),
                    UI.getUserInput(Constants.COUNTRY),
                    UI.getUserInput(Constants.LANDMARK));
    }

    /**
     * <p>
     * Getting a set of address details from user which are validated.
     * </p>
     * @return addresses which has address details entered by user.
     */
   public Set<Address> getValidAddresses() {
       Set<Address> addresses = new HashSet<Address>();
       AddressController addressController = new AddressController();
       Address address = null;
       do {
           address = addressController.validateAddress
                                                (getAddressDetailsFromUser());
           while (address.getId() < 0) {
               address.setId(0);
               address = getValidAddressDetailsFromUser(address);
               address = addressController.validateAddress(address);
           }
           addresses.add(address);
       } while(UI.getConfirmationFromUser(Constants.WANT_ADD));
       return addresses;
   }

   /**
     *<p>
     * Getting valid address details from user.
     *</p>
     * @return address which has valid address details entered by user.
     */
    public Address getValidAddressDetailsFromUser(Address address) {
        displayAddress(address);
        if (address.getName().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            address.setName(UI.getUserInput(Constants.HOLDER_NAME));
        }
        if (address.getAddressOne().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.DONT);
            address.setAddressTwo(UI.getUserInput(Constants.ADDRESS_ONE));
        }
        if (address.getAddressTwo().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.DONT);
            address.setAddressTwo(UI.getUserInput(Constants.ADDRESS_TWO));
        }
        if (address.getCity().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.NAME);
            address.setCity(UI.getUserInput(Constants.CITY));
        }
        if (address.getZipcode().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.DOB);
            address.setZipcode(UI.getUserInput(Constants.ZIP));
        }
        if (address.getState().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.EMAIL);
            address.setState(UI.getUserInput(Constants.STATE));
        }
        if (address.getCountry().equals(Constants.INVALID)) {
            showCorrectFormat(Constants.SALARY);
            address.setCountry(UI.getUserInput(Constants.COUNTRY));
        }
        return address;
    }

    /**
     *<p>
     * Displaying details of the given address to the user.
     *</p>
     * @param address - which has details to be displayed to the user.
     */
    public void displayAddress(Address address) {
        if ( 0 == address.getId()) {
            UI.display(String.format(Constants.MSG_NOT_EXIST, Constants.
                          HOLDER_ID));
        } else {
            UI.display(String.format(Constants.DISPLAY_ADDRESS, address.
                           getId(), address.getName(),
                           address.getAddressOne(), address.getAddressTwo(),
                           address.getCity(), address.getZipcode(),
                           address.getState(), address.getCountry(),
                           address.getLandMark()));
        }
    }

    public void displayAddresses(Set<Address> addresses) {
        UI.display("List of Addresses::");
        for (Address address : addresses) {
            displayAddress(address);
        }
    }

    /**
     *<p>
     * Showing some text to user to indicate them valid format of the given 
     * Attribute.
     *</p>
     */
    public void showCorrectFormat(String attribute) {
        showMessage(Constants.MSG_ADDRESS_FORMAT);
    }

    /**
     *<p>
     * Showing user a set of options to choose for updation.
     *</p>
     */
    public void showAddressUpdateMenu() {
        UI.display(Constants.ADDR_UPD_MENU);
    }

    /**
     *<p>
     * Showing user by the given message.
     *</p>
     */
    public void showMessage(String message) {
        UI.display(message);
    }

     /**
      *<p>
      * show address crud operation
      *</p>
      */
     public Set<Address> showAddressCRUD(Set<Address> addresses) {
        if (null == addresses)
            addresses = new HashSet<Address>();
        return new AddressController().doAddressCRUD(addresses);
     }

    /**
     * Showing user by the given messages.
     */
    public void showMessage(List<String> messages) {
        int count = 1;
        for(String message: messages)
        UI.display(count++ + ". "+message);
    }
}
