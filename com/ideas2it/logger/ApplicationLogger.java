package com.ideas2it.logger;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.ideas2it.common.Constants;

/**
 * <p>
 * Implementing custom exception by providing meaningfull information when an 
 * abnormal problem occurs due to User input.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class ApplicationLogger {

   private static Logger logger;

   private static Logger getLogger(String className) {
       return Logger.getLogger(className);
   }

   public static void error(String msg, Throwable cause) {
       logger = getLogger((cause.getStackTrace())[0].getClassName());
       DOMConfigurator.configure(Constants.LOG4J_CONFIG_FILE);
       logger.error(msg, cause);
   }

   public static void debug(String msg, Throwable cause) {
       logger = getLogger((cause.getStackTrace())[0].getClassName());
       DOMConfigurator.configure(Constants.LOG4J_CONFIG_FILE);
       logger.debug(msg, cause);
   }

   public static void info(String msg, Throwable cause) {
       logger = getLogger((cause.getStackTrace())[0].getClassName());
       DOMConfigurator.configure(Constants.LOG4J_CONFIG_FILE);
       logger.info(msg, cause);
   }
}
