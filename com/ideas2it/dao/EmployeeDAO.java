package com.ideas2it.dao;

import java.util.List;
import java.sql.ResultSet;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Employee;

/**
 * <p>
 * Data access object for employee entity.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public interface EmployeeDAO {

    /**
     * <p>
     * Inserting Employee details into the employee table.
     * </p>
     *
     * @param employee - which contains the details of an employee.
     *
     * @return the number of employees inserted.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            an employee detail.
     */
    public void updateEmployee(Employee employee, char type)
        throws ApplicationException;

	/**
     * <p>
     * Reading employee details from the employee table by the given id.
     * </p>
     * 
     * @param employeeId - by which the details of an employee is read from the 
     *                     table.
     *
     * @return employee which is of the given emailId.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            an employee detail by the given employee id.
     */
	public Employee readEmployeeById(int employeeId) throws
        ApplicationException;

	/**
     * <p>
     * Reading employee's by the given project id.
     * </p>
     * 
     * @param projectId - by which the details of an employee is read.
     *
     * @return employees which has the given project id.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            all the employees in a project by the given project id.
     */
    public List<Employee> readEmployeesByProjectId(int projectId) throws
        ApplicationException;

	/**
     * <p>
     * Reading all the employee's.
     * </p>
     *
     * @return employees which has the given project id.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading
     *            all the employees in a project by the given project id.
     */
    public List<Employee> readAllEmployees() throws ApplicationException;
}
