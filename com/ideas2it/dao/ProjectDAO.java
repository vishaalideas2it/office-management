package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Project;

/**
 * <p>
 * Data access object for Project entity.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public interface ProjectDAO {

    /**
     * <p>
     * Inserting Project details into the project table.
     * </p>
     *
     * @param project - which contains the details of a project.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            a project.
     */
    public void updateProject(Project project, char type) throws ApplicationException;

    /**
     * <p>
     * Reading Project details from the project table by the given project id.
     * </p>
     *
     * @param projectId - by which the project detail is read from the table.
     *
     * @return the details of a project.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading a
     *            project detail by the given project id.
     */
    public Project readProjectById(int projectId) throws
        ApplicationException;

    /**
     * <p>
     * Reading Project details from the project table by the given project id.
     * </p>
     *
     * @param projectId - by which the project detail is read from the table.
     *
     * @return the details of all existing project.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading all
     *            the projects.
     */
    public List<Project> readAllProject() throws ApplicationException;

    /**
     * <p>
     * Reading Project id from the project table by the given project name.
     * </p>
     *
     * @param projectId - by which the project id is read from the table.
     *
     * @return id which is of the given project name.
     *
     * @exception ApplicationException when executing the query statement and 
     *            reading from result set.
     */
    public Project readProjectIdByName(String name) throws ApplicationException;
}
