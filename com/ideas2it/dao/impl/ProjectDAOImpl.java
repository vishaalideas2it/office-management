package com.ideas2it.dao.impl;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Query;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.ConnectionFactory;
import com.ideas2it.dao.ProjectDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Client;
import com.ideas2it.model.Project;
import com.ideas2it.connection.SessionFactory;

/**
 * <p>
 * Implementing a data access object for the entity project to do CRUD 
 * operation in database. Such as inserting details of an project into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * Author : Vishaal 
 * Date   : 16.8.2017
 */
public class ProjectDAOImpl implements ProjectDAO {

    /**
     * @see com.ideas2it.dao.ProjectDAO#insertProject(Project).
     */
    public void updateProject(Project project, char type)
            throws ApplicationException {
        Session session =  SessionFactory.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            if (Constants.INSERTION == type)
                session.save(project);
            else if (Constants.UPDATION == type)
                session.update(project);
            else if (Constants.DELETION == type)
                session.delete(project);
            transaction.commit();
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            transaction.rollback();
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#readProjectById(String).
     */
    public Project readProjectById(int projectId) throws
        ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery(String.format("from Project "
                                      + "where id = '%s'", projectId));
             return ((Project)query.uniqueResult());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#readAllProject().
     */
    public List<Project> readAllProject() throws ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery("from Project");
             return ((List<Project>)query.list());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.ProjectDAO#readProjectIdByName(String).
     */
    public Project readProjectIdByName(String name) throws
        ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery(String.format("from Project "
                                      + "where name = '%s'", name));
             return ((Project)query.uniqueResult());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }
}
