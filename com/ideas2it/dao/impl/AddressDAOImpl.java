package com.ideas2it.dao.impl;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Query;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.SessionFactory;
import com.ideas2it.dao.AddressDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a data access object for the entity address to do CRUD 
 * operation in database. Such as inserting details of an address into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * Author : Vishaal 
 * Date   : 16.8.2017
 */
public class AddressDAOImpl implements AddressDAO {

    /**
     * @see com.ideas2it.dao.AddressDAO#insertAddress(Address).
     */
    public void updateAddress(Address address, char type) throws
            ApplicationException {
        Session session =  SessionFactory.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            if (Constants.INSERTION == type)
                session.save(address);
            else if (Constants.UPDATION == type)
                session.update(address);
            else if (Constants.DELETION == type)
                session.delete(address);
            transaction.commit();
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            transaction.rollback();
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }
}
