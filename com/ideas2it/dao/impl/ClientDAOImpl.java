package com.ideas2it.dao.impl;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Query;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.SessionFactory;
import com.ideas2it.dao.ClientDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a data access object for the entity client to do CRUD 
 * operation in database. Such as inserting details of an client into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * Author : Vishaal
 * Date   : 16.8.2017
 */
public class ClientDAOImpl implements ClientDAO {

    /**
     * @see com.ideas2it.dao.clientDAO#insertclient(client).
     */
    public void manipulateClient(Client client, char type)
            throws ApplicationException {
        Session session =  SessionFactory.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            if (Constants.INSERTION == type)
                session.save(client);
            else if (Constants.UPDATION == type)
                session.update(client);
            else if (Constants.DELETION == type)
                session.delete(client);
            transaction.commit();
        } catch(HibernateException e) {
            ApplicationLogger.error(getErrorMsg(type) + client, e);
            transaction.rollback();
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.clientDAO#readclientById(String).
     */
    public Client readClientById(int clientId) throws
        ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery(String.format("from Client "
                                      + "where id = '%s'", clientId));
             return ((Client)query.uniqueResult());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.clientDAO#readAllclient().
     */
    public List<Client> readAllClient() throws ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery("from Client");
             return ((List<Client>)query.list());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.clientDAO#readclientIdByName(String).
     */
    public Client readClientByName(String name) throws
        ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery(String.format("from Client "
                                      + "where name = '%s'", name));
             return ((Client)query.uniqueResult());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * Get an  error msg in which type
     */
    public String getErrorMsg(char type) {
        String msg = " ";
        if (Constants.INSERTION == type) {
            msg = Constants.ERR_INS + Constants.CLIENT;
        } else if (Constants.UPDATION == type) {
            msg = Constants.ERR_UPD + Constants.CLIENT;
        } else if (Constants.DELETION == type) {
            msg = Constants.ERR_DEL + Constants.CLIENT;
        }
        return msg;
    }
}
