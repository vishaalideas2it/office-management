package com.ideas2it.dao.impl;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Query;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.SessionFactory;
import com.ideas2it.dao.EmployeeDAO;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;

/**
 * <p>
 * Implementing a data access object for the entity employee to do CRUD 
 * operation in database. Such as inserting details of an employee into the
 * table. It gives access to the service class for accessing the database.
 * </p>
 * Author : Vishaal
 * Date   : 16.8.2017
 */
public class EmployeeDAOImpl implements EmployeeDAO {

    /**
     * @see com.ideas2it.dao.ProjectDAO#insertProject(Project).
     */
    public void updateEmployee(Employee employee, char type)
            throws ApplicationException {
        Session session =  SessionFactory.getSession();
        Transaction transaction = session.beginTransaction();
        try {
            if (Constants.INSERTION == type)
                session.save(employee);
            else if (Constants.UPDATION == type)
                session.update(employee);
            else if (Constants.DELETION == type)
                session.delete(employee);
            transaction.commit();
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            transaction.rollback();
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#readEmployeeById(String).
     */
    public Employee readEmployeeById(int employeeId) throws
                                                     ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery(String.format("from Employee "
                                      + "where id = %d", employeeId));
             return ((Employee)query.uniqueResult());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#readEmployeeById(String).
     */
    public List<Employee> readAllEmployees() throws ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery("from Employee");
             return ((List<Employee>)query.list());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }

    /**
     * @see com.ideas2it.dao.EmployeeDAO#readEmployeesByProjectId(String).
     */
    public List<Employee> readEmployeesByProjectId(int projectId) throws
                                                      ApplicationException {
        Session session =  SessionFactory.getSession();
        try {
            Query query = session.createQuery(String.format("from Employee "
                                      + "where project_id = %d", projectId));
             return ((List<Employee>)query.list());
        } catch(HibernateException e) {
            ApplicationLogger.error(Constants.CLIENT_INS_ERR, e);
            throw new ApplicationException(e);
        } finally {
            session.close();
        }
    }
}
