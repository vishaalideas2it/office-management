package com.ideas2it.dao;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Client;

/**
 * <p>
 * Data access object for client entity.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */

public interface ClientDAO {
    /**
     * <p>
     * Inserting client details into the client table.
     * </p>
     *
     * @param client - which contains the details of a client.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            a client.
     */
    public void manipulateClient(Client client, char type)
        throws ApplicationException;

    /**
     * <p>
     * Reading client details from the client table by the given client id.
     * </p>
     *
     * @param clientId - by which the client detail is read from the table.
     *
     * @return the details of a client.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading a
     *            client detail by the given client id.
     */
    public Client readClientById(int clientId) throws
        ApplicationException;

    /**
     * <p>
     * Reading client details from the client table by the given client id.
     * </p>
     *
     * @param clientId - by which the client detail is read from the table.
     *
     * @return the details of all existing client.
     *
     * @exception ApplicationException - if a SQL error occurs, when reading all
     *            the clients.
     */
    public List<Client> readAllClient() throws ApplicationException;

    /**
     * <p>
     * Reading client id from the client table by the given client name.
     * </p>
     *
     * @param clientId - by which the client id is read from the table.
     *
     * @return id which is of the given client name.
     *
     * @exception ApplicationException when executing the query statement and 
     *            reading from result set.
     */
    public Client readClientByName(String name) throws ApplicationException;
}
