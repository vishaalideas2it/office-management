package com.ideas2it.dao;

import java.util.List;
import java.sql.ResultSet;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.model.Address;

/**
 * <p>
 * Data access object for address entity.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public interface AddressDAO {

    /**
     * <p>
     * Inserting Address details into the address table.
     * </p>
     *
     * @param address - which contains the details of an address.
     *
     * @return the number of addresss inserted.
     *
     * @exception ApplicationException - if a SQL error occurs, when inserting
     *            an address detail.
     */
    public void updateAddress(Address address, char type)
        throws ApplicationException;

}
