package com.ideas2it.controller;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.common.Constants;
import com.ideas2it.connection.SessionFactory;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;
import com.ideas2it.model.Project;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.service.ClientService;
import com.ideas2it.view.AddressView;
import com.ideas2it.view.ClientView;
import com.ideas2it.view.ProjectView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for Client Details which interacts with the user.
 * The user can add, create, update and delete the detail of an Client.
 * </p>
 * Author : Vishaal
 * Date   : 16.8.2017
 */
class ClientController {

    public ClientService clientService = new ClientServiceImpl();
    private ClientView clientView = new ClientView();

    /**
     * <p>
     * Adding a new Client to Clients.
     * </p>
     */
    public void addNewClient() {
        try {
            Client client = clientView.getClientDetailsFromUser();
            if (UI.getConfirmationFromUser(Constants.CREATE_PROJECT)) {
                client.setProjects(new ProjectView().getValidProjectsFromUser());
            }
            if (UI.getConfirmationFromUser()) {
                client.setAddresses(new AddressView().getValidAddresses());
            }
            addClient(client, Constants.INSERTION);
            clientView.showMessage(Constants.NO_CLIENT);
        } catch(ApplicationException e) {
            clientView.showMessage(Constants.UNABLE_TO_CREATE);
        }
    }

    /**
     * <p>
     * Adding an Client details to Client's records or updating details of an
     * existing Client. The operation's such as insertion and updation are
     * classified by the given type.
     *
     * @param client - which is to be added or updated.
     * @param type -  which specifies the type of operation.
     *
     * @exception ApplicationException - if SQL error occurs while doing the
                                         operation specified by the type.
     * </p>
     */
    public void addClient(Client client, char type)
            throws ApplicationException{
        boolean notInserted = true;
        do {
            try {
                clientService.addClient(client, type);
                notInserted = false;
            } catch(UserInputException e) {
                notInserted = true;
                ApplicationLogger.error(Constants.PROJ_UPD_ERR +
                                           client.getId(), e);
                client = clientView.getValidClientDetailsFromUser
                             ((Client)e.getObject());
            }
        } while(notInserted);
    }

    /**
     * <p>
     * Deleting an Client from Clients by the id given by user.
     * </p>
     */
    public void deleteClient() {
        int id = UI.getUserInputNumber(Constants.CLIENT_ID);
        boolean deleted = false;
        try {
            if (UI.getConfirmationFromUser(Constants.WANT_DELETE) &&
                    clientService.deleteClientById(id))
                deleted = true;
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_DEL_ERR + id, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
        if (deleted) {
            clientView.showMessage(String.format(Constants.MSG_DELETION,
                                         Constants.CLIENT, id));
        } else {
            clientView.showMessage(String.format(Constants.MSG_NOT_EXIST,
                                               Constants.CLIENT, id));
        }
    }

    /**
     * <p>
     * Updating the deatils of the given client by letting the user to choose
     * what to update and what data is to be updated.
     * </p>
     *
     * @return client - which contains the updated values given by the user.
     */
    public Client getUpdationDetailsFromUser(Client client) {
        int choice = 0;
        do {
            clientView.showClientUpdateMenu();
            choice = UI.getUserInputNumber(Constants.CHOICE);
            switch(choice) {
                case 1:
                    client.setName(UI.getUserInput(Constants.NAME));
                    break;
                case 2:
                    client.setEmailId(UI.getUserInput(Constants.EMAIL));
                    break;
                case 3:
                    client.setAddresses(new AddressView().doAddressCRUD(
                                           client.getAddresses()));
                case 0:
                    return client;
                default:
                    clientView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(0 != choice);
        return client;
    }

    /**
     * <p>
     * Updating an Client's attribute by Client id(given by user).
     * </p>
     */
    public void updateClientDetails() {
        Client client = clientService.getClientById
                            (UI.getUserInputNumber(Constants.CLIENT_ID));
        while(null == client) {
            clientView.showMessage(Constants.ID_EXCITING);
            client = clientService.getClientById
                         (UI.getUserInputNumber(Constants.CLIENT_ID));
        }
        try {
            addClient(getUpdationDetailsFromUser(client), Constants.UPDATION);
            clientView.showMessage(Constants.UPDATE_SUCCESS);
        } catch(ApplicationException e) {
            clientView.showMessage(Constants.EXCITING_UPDATION);
        }
    }

    /**
     * <p>
     * Getting client Id by the given client name.
     * </p>
     *
     * @param clientName - for which client id is searched.
     *
     * @return the client id.
     */
    public int getIdByClientName(String ClientName) {
        try {
            return clientService.getIdByClientName(ClientName);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ID_ERR + ClientName, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
        return 0;
    }

    /**
     * <p>
     * Displaying all the existing clients.
     * </p>
     */
    public void displayAllClients() {
        try {
            for(Client client : clientService.getAllClients())
                clientView.displayClient(client);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ALL_ERR, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Displaying a client by the id given by .
     * </p>
     */
    public void displayClientById() {
        try {
            clientView.displayClient
                        (clientService.getClientById(UI.
                                       getUserInputNumber(Constants.CLIENT_ID)));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ERR, e);
            clientView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Controlling the CRUD operation on Client details based on the choice 
     * of user.
     * </p>
     */
    public void showClientCRUD() { 
        int choice;
        do {
            clientView.showClientCRUDMenu();
            choice = Integer.parseInt(UI.getUserInput(Constants.CHOICE));

            switch(choice) {
                case 1:
                    addNewClient();
                    break;
                case 2:
                    displayClientById();
                    break;
                case 3:
                    updateClientDetails();
                    break;
                case 4:
                    deleteClient();
                    break;
                case 0:
                    SessionFactory.closeSessionFactory();
                    return;
                default:
                    clientView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }

    public static void main(String args[]) {
        new ClientController().showClientCRUD();
    }
}
