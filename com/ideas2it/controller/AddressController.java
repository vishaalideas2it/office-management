package com.ideas2it.controller;

import java.util.Set;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.ProjectController;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;
import com.ideas2it.model.Employee;
import com.ideas2it.service.AddressService;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.ClientView;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.view.AddressView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for Address Details which interacts with the user.
 * The user can add, create, update and delete the detail of an address.
 * </p>
 * Author : Vishaal
 * Date   : 16.8.2017
 */
public class AddressController {
    private AddressService addressService = new AddressServiceImpl();
    private AddressView addressView = new AddressView();

    /**
     * <p>
     * Adding a new address to addresss.
     * </p>
     */
    public void addNewAddress(Address address) {
        Address newAddress = addressView.getAddressDetailsFromUser();
        newAddress.setEmployee(address.getEmployee());
        newAddress.setClient(address.getClient());
        addAddress(newAddress, Constants.INSERTION);
        addressView.showMessage(String.format(Constants.MSG_SUCCESS, 
                                     Constants.EMP_ID));
    }

    /**
     * <p>
     * Doing the operation specified by the updation type. The operations are
     * Insertion and updation.
     * </p>
     */
    public void addAddress(Address address, char updationType)
            throws ApplicationException{
        boolean notInserted = true;
        do {
            try {
                addressService.addAddress(address, updationType);
                notInserted = false;
            } catch(UserInputException e) {
                notInserted = true;
                ApplicationLogger.error(Constants.PROJ_UPD_ERR +
                                           address.getId(), e);
                address = addressView.getValidAddressDetailsFromUser
                             ((Address)e.getObject());
            }
        } while(notInserted);
    }

    /**
     * <p>
     * Deleting an address from addresss by the id given by user.
     * </p>
     */
    public void deleteAnAddress(Set<Address> addresses) {
        Address address = getAddressById(addresses);
        if (address.getId() != 0 && UI.
                            getConfirmationFromUser(Constants.DELETE_DETAIL)) {
            try {
                    addresses.remove(address);
                    addressService.deleteAddress(address);
                    addressView.showMessage(String.format(Constants.
                                  MSG_DELETION,Constants.EMPLOYEE, null));
            } catch(ApplicationException e){
                ApplicationLogger.error(Constants.EMP_DEL_ERR, e);
                addressView.showMessage(Constants.USR_ERR_UNABLE);
            }
        } else {
             UI.display(Constants.DELETE_EXCITING);
        }
    }

    /**
     * <p>
     * Getting an address from a set of addresss by the id given by user.
     * </p>
     */
    public Address getAddressById(Set<Address> addresses) {
        if (addresses.size() == 0) {
            UI.display(Constants.NO_ADDRESS);
            return new Address();
        }
        do {
            int addressId = UI.getUserInputNumber(Constants.ID);
            addressView.displayAddresses(addresses);
            for (Address address : addresses) {
                if (address.getId() == addressId) {
                    return address;
                }
            }
        } while(UI.getConfirmationFromUser(Constants.ID_EXCITING));
        return new Address();
    }

    /**
     * <p>
     * Deleting an address from addresss by the id given by user.
     * </p>
     */
    public void addAddresses(Set<Address> addresses) {
        Set<Address> newAddresses = addressView.getValidAddresses();
        for (Address address : newAddresses)
            addresses.add(address);
    }

    /**
     * <p>
     * Updating an address's attribute by address id(given by user).
     * </p>
     */
    public void updateAddressDetails(Set<Address> addresses) {
        Address address = getAddressById(addresses);
        if (address.getId() != 0) {
             addresses.add(getUpdationDetailsFromUser(address));
        } else {
             UI.display(Constants.EXCITING_UPDATION);
        }
    }

    /**
     *<p>
     * Address Validation 
     *</p>
     */
    public Address validateAddress(Address address) {
        return addressService.validateDetailsOfAddress(address);
    }


    /**
     * <p>
     * Controlling the CRUD operation on address details based on the choice 
     * given by the user.
     * </p>
     */
    public Set<Address> showAddressCRUD(Set<Address> addresses) {
        int choice = 0;
        do {
            addressView.showAddressCRUDMenu();
                choice = UI.getUserInputNumber(Constants.CHOICE);
            switch(choice) {
                case 1:
                    addAddresses(addresses);
                    break;
                case 2:
                    addressView.displayAddresses(addresses);
                    break;
                case 3:
                    updateAddressDetails(addresses);
                    break;
                case 4:
                    deleteAnAddress(addresses);
                    break;
                case 0:
                    break;
                default:
                    addressView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
        return addresses;
    }

    /**
     * <p>
     * Updating the deatils of the given address by letting the user to choose
     * what to update and what data is to be updated.
     * </p>
     *
     * @return address - which contains the updated values given by the user.
     */
    public Address getUpdationDetailsFromUser(Address address) {
        int choice;
        do {
            addressView.showAddressUpdateMenu();
            choice = Integer.parseInt(UI.getUserInput(Constants.CHOICE));
            switch(choice) {
                case 1:
                    address.setName(UI.getUserInput(Constants.HOLDER_NAME));
                    break;
                case 2:
                    address.setAddressOne(UI.getUserInput(Constants.ADDRESS_ONE));
                    break;
                case 3:
                    address.setAddressTwo(UI.getUserInput(Constants.ADDRESS_TWO));
                    break;
                case 4:
                    address.setCity(UI.getUserInput(Constants.CITY));
                    break;
                case 5:
                    address.setZipcode(UI.getUserInput(Constants.ZIP));
                    break;
                case 6:
                    address.setState(UI.getUserInput(Constants.STATE));
                    break;
                case 7:
                    address.setCountry(UI.getUserInput(Constants.COUNTRY));
                    break;
                case 8:
                    address.setLandMark(UI.getUserInput(Constants.LANDMARK));
                    break;
                case 0:
                    return address;
                default:
                    addressView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(0 != choice);
        return address;
    }
}
