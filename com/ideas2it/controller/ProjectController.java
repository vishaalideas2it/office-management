package com.ideas2it.controller;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.ProjectView;
import com.ideas2it.view.UI;

/**
 * <p>
 * Implementing a Controller for project Details which interacts with the user.
 * The user can add, create, update and delete the detail of an project.
 * </p>
 * Author : Vishaal 
 * Date   : 16.8.2017
 */
public class ProjectController {

    public ProjectService projectService = new ProjectServiceImpl();
    private ProjectView projectView = new ProjectView();

    /**
     * <p>
     * Adding a new project to projects record.
     * </p>
     */
    public void addProject(Project project, char type)
            throws ApplicationException{
        boolean notInserted = true;
        do {
            try {
                projectService.addProject(project, type);
                notInserted = false;
            } catch(UserInputException e) {
                notInserted = true;
                ApplicationLogger.error(Constants.PROJ_UPD_ERR +
                                           project.getId(), e);
                project = projectView.getValidProjectDetailsFromUser
                             ((Project)e.getObject());
            }
        } while(notInserted);
    }
    /**
     * <p>
     * Adding a new Client to Clients record.
     * </p>
     */
    public void addNewProject() {
        try {
            Project project = projectView.getProjectDetailsFromUser();
            if (UI.getConfirmationFromUser(Constants.CREATE_EMPLOYEE)) {
                project.setEmployees(new EmployeeView().getValidEmployeesFromUser());
            }
            addProject(project, Constants.INSERTION);
            projectView.showMessage(Constants.NEW_EMPLOYEE);
        } catch(ApplicationException e) {
            projectView.showMessage(Constants.UNABLE_TO_CREATE);
        }
    }

    /**
     * <p>
     * Deleting an project from projects by the id given by user.
     * </p>
     */
    public void deleteProject() {
        int id = UI.getUserInputNumber(Constants.PROJECT_ID);
        boolean deleted = false;
        try {
            if (UI.getConfirmationFromUser(Constants.WANT_DELETE) && projectService.
                   deleteProjectById(id))
                deleted = true;
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_DEL_ERR + id, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
        if (deleted) {
            projectView.showMessage(String.format(Constants.MSG_DELETION,
                                         Constants.PROJECT, id));
        } else {
            projectView.showMessage(String.format(Constants.MSG_NOT_EXIST,
                                               Constants.PROJECT, id));
        }
    }

    /**
     * <p>
     * Updating an project's attribute by project id(given by user).
     * </p>
     */
    public void updateProjectDetails() {
        Project project = null;
        boolean notExist = false;
        do {
            if (notExist)
                projectView.showMessage(Constants.ID_EXCITING);
            project = projectService.getProjectById
                         (UI.getUserInputNumber(Constants.CLIENT_ID));
            notExist = true;
        } while(null == project);
        try {
            addProject(getUpdationDetailsFromUser(project), Constants.UPDATION);
            projectView.showMessage(Constants.UPDATE_SUCCESS);
        } catch(ApplicationException e) {
            projectView.showMessage(Constants.EXCITING_UPDATION);
        }
    }

    public Project getUpdationDetailsFromUser(Project project) {
        int choice = 0;
        do {
            projectView.showProjectUpdateMenu();
            choice = UI.getUserInputNumber(Cosntants.CHOICE);
            switch(choice) {
                case 1:
                    project.setName(UI.getUserInput(Constants.NAME));
                    break;
                case 0:
                    return project;
                default:
                    projectView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(0 != choice);
        return project;
    }

    public Project validateProject(Project project) {
        return projectService.validateProjectDetails(project);
    }

    /**
     * <p>
     * Getting a project id by the given project name.
     *
     * @param projectName by which project id is read.
     *
     * @return project id which got by the project name.
     * </p>
     */
    public int getIdByProjectName(String projectName) {
        try {
            return projectService.getIdByProjectName(projectName);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ID_ERR + projectName, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
        return 0;
    }

    /**
     * <p>
     * Displaying all the existing projects.
     * </p>
     */
    public void displayAllProjects() {
        try {
            for(Project project : projectService.getAllProjects())
                projectView.displayProject(project);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ALL_ERR, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Assigning a employee to an project.
     * </p>
     *
     * @param id - is the employee whoose attribute is updated.
     * @param attribute - which is updated in employee.
     * @param  status - indicates whether the attribute is updated or not.
     */
    public void assignEmployeesToProject() {
        UI.display("List of Projects ::");
        displayAllProjects();
        String projectId = UI.getUserInput(Constants.PROJECT_ID);
        UI.display("List of Employees ::");
        projectView.showMessage(projectService.getAllEmployees());
        try {
             projectService.assignEmployeesToProject(UI.getUserInput
                       (Constants.EMPLOYEE_ID),  projectId);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        } catch(UserInputException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            projectView.showMessage(e.getErrorCode()+Constants.MSG_NOT_EXIST);
            assignEmployeesToProject();
            return;
        }
    }

    /**
     * <p>
     * Displaying a project by the id given by user..
     * </p>
     */
    public void displayProjectById() {
        try {
            projectView.displayProject
                        (projectService.getProjectById(UI.
                                    getUserInputNumber(Constants.PROJECT_ID)));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.PROJ_RD_ERR, e);
            projectView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Controlling the CRUD operation on project details based on the choice 
     * of user.
     * </p>
     */
    public void showProjectCRUD() { 
        int choice;
        do {
            projectView.showProjectCRUDMenu();
            choice = Integer.parseInt(UI.getUserInput(Constants.CHOICE));

            switch(choice) {
                case 1:
                    addNewProject();
                    break;
                case 2:
                    displayProjectById();
                    break;
                case 3:
                    updateProjectDetails();
                    break;
                case 4:
                    deleteProject();
                case 5:
                    assignEmployeesToProject();
                    break;
                case 0:
                    return;
                default:
                    projectView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }
}
