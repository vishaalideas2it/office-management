package com.ideas2it.controller;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.common.Constants;
import com.ideas2it.controller.ProjectController;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.logger.ApplicationLogger;
import com.ideas2it.model.Address;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.view.AddressView;
import com.ideas2it.view.EmployeeView;
import com.ideas2it.view.UI;

/**
 * <p>
 * A Controller for Employee Details which interacts with the user Interface.
 * The user can add, create, update and delete the detail of an employee.
 * </p>
 * Author : Vishaal
 * Date   : 16.8.2017
 */
public class EmployeeController {
    private EmployeeService employeeService = new EmployeeServiceImpl();
    private EmployeeView employeeView = new EmployeeView();

    /**
     * <p>
     * Displaying employee's in a project by the project id given by user.
     * </p>
     * 
     * @param projectId - by which the employees are choosen and displayed.
     */
    public void displayEmployeesByProjectId(int projectId) {
        try {
           for (Employee employee : employeeService
                                      .getEmployeesByProjectId(projectId))
            employeeView.displayEmployee(employee);
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ALL_PROJ_ERR
                                      + projectId, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
       }
    }

    /**
     * <p>
     * Validating the details of the given employee.
     * </p>
     *
     * @return employee - for which the details have been validated.
     */
    public Employee validateEmployee(Employee employee) {
        return employeeService.validateDetailsOfEmployee(employee);
    }

    /**
     * <p>
     * Adding a new Employee to Employees record.
     * </p>
     */
    public void addNewEmployee() {
        try {
        Employee employee = employeeView.getEmployeeDetailsFromUser();
        employee.setAddresses(new AddressView().getValidAddresses());
        addEmployee(employee, Constants.INSERTION);
        employeeView.showMessage(Constants.NEW_EMPLOYEE);
        } catch(ApplicationException e) {
            employeeView.showMessage(Constants.UNABLE_TO_CREATE);
        }
    }

    /**
     * <p>
     * Adding an employee details to employees records or updating details of an
     * existing employee. The operation's such as insertion and deletion are
     * classified by the given type.
     *
     * @param employee - which is to be added or updated.
     * @param type -  which specifies the type of operation.
     *
     * @exception ApplicationException - if SQL error occurs while doing the
                                         operation specified by the type.
     * </p>
     */
    public void addEmployee(Employee employee, char type)
            throws ApplicationException{
        boolean notInserted = true;
        do {
            try {
                employeeService.addEmployee(employee, type);
                notInserted = false;
            } catch(UserInputException e) {
                notInserted = true;
                ApplicationLogger.error(Constants.PROJ_UPD_ERR +
                                           employee.getId(), e);
                employee = employeeView.getValidEmployeeDetailsFromUser
                             ((Employee)e.getObject());
            }
        } while(notInserted);
    }

    /**
     * <p>
     * Displaying the details of all the employees existing in the record.
     */
    public void displayAllEmployees() {
        try {
            employeeView.showMessage(employeeService.getAllEmployees());
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ALL_PROJ_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        }
    }

    /**
     * <p>
     * Deleting an employee from employees record by the id given by user.
     * </p>
     */
    public void deleteAnEmployee() {
        int id = UI.getUserInputNumber(Constants.EMPLOYEE_ID);
        if (UI.getConfirmationFromUser(Constants.WANT_DELETE)) {
            try {
                if (employeeService.deleteEmployeeById(id)) {
                    employeeView.showMessage(String.format(Constants.
                                         MSG_DELETION,Constants.EMPLOYEE, id));
                } else {
                    employeeView.showMessage(String.format(Constants.
                                         MSG_NOT_EXIST,Constants.EMPLOYEE, id));
                }
            } catch(ApplicationException e){
                ApplicationLogger.error(Constants.EMP_DEL_ERR + id, e);
                employeeView.showMessage(Constants.USR_ERR_UNABLE);
            }
        } 
    }
                
    /**
     * <p>
     * Updating an Employee's detail.
     * </p>
     */
    public void updateEmployeeDetails() {
        displayAllEmployees();
        Employee employee = employeeService.getEmployeeById
                            (UI.getUserInputNumber(Constants.EMPLOYEE_ID));
        while(null == employee) {
            displayAllEmployees();
            employeeView.showMessage(Constants.ID_EXCITING);
            employee = employeeService.getEmployeeById
                         (UI.getUserInputNumber(Constants.EMPLOYEE_ID));
        }
        try {
            addEmployee(getUpdationDetailsFromUser(employee), Constants.UPDATION);
            employeeView.showMessage(Constants.UPDATE_SUCCESS);
        } catch(ApplicationException e) {
            employeeView.showMessage(Constants.EXCITING_UPDATION);
        }
    }

    /**
     * <p>
     * Assigning a project to an employee.
     * </p>
     *
     */
    public void assignProjectToEmployee() {
        boolean notUpdated = false;
        UI.display("List of Employees ::");
        displayAllEmployees();
        int employeeId = UI.getUserInputNumber(Constants.EMPLOYEE_ID);
        UI.display("List of Projects ::");
        employeeView.showMessage(employeeService.getProjectNames());
        try {
            employeeService.assignProjectToEmployee(employeeId,
                            UI.getUserInputNumber(Constants.PROJECT_ID));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        } catch(UserInputException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            employeeView.showMessage(e.getErrorCode()+Constants.MSG_NOT_EXIST);
            assignProjectToEmployee();
        }
            employeeView.showMessage(Constants.MSG_PROCESS_DONE);
    }

    /**
     * <p>
     * Removing project from an employee.
     * </p>
     *
     */
    public void unassignProjectFromEmployee() {
        UI.display("List of Employees ::");
        displayAllEmployees();
        int id = UI.getUserInputNumber(Constants.EMPLOYEE_ID);
        try {
            if (UI.getConfirmationFromUser()) {
                employeeService.unassignProjectFromEmployee(id);
                employeeView.showMessage(Constants.MSG_PROCESS_DONE);
            }
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_UNASSIGN_PROJ_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        } catch(UserInputException e) {
            ApplicationLogger.error(Constants.EMP_ASSIGN_PROJ_ERR, e);
            employeeView.showMessage(e.getErrorCode() +
                                        Constants.MSG_NOT_EXIST);
            unassignProjectFromEmployee();
        }
    }

    /**
     * <p>
     * Displaying details of an Employee by the id given by user.
     * </p>
     */
    public void displayEmployee() {
        try {
            employeeView.displayEmployee
                        (employeeService.getEmployeeById(UI.
                            getUserInputNumber(Constants.EMP_ID)));
        } catch(ApplicationException e) {
            ApplicationLogger.error(Constants.EMP_RD_ERR, e);
            employeeView.showMessage(Constants.USR_ERR_UNABLE);
        }
    } 

    /**
     * <p>
     * Controlling the CRUD operation on employee details based on the choice 
     * given by the user.
     * </p>
     */
    public void showEmployeeCRUD() {
        int choice = 0;
        do {
            employeeView.showEmployeeCRUDMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput(Constants.CHOICE));
            } catch(NumberFormatException e) {
                choice = -1;
            }
            switch(choice) {
                case 1:
                    addNewEmployee();
                    break;
                case 2:
                    displayEmployee();
                    break;
                case 3:
                    updateEmployeeDetails();
                    break;
                case 4:
                    deleteAnEmployee();
                    break;
                case 5:
                    assignProjectToEmployee();
                    break;
                case 6:
                    unassignProjectFromEmployee();
                    break;
                case 7:
                    displayEmployeesByProjectId(employeeService.
                        getProjectIdByName(UI.getUserInput
                             (Constants.PROJECT_NAME)));
                    break;
                case 0:
                    return;
                default:
                    employeeView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }

    /**
     * <p>
     * Controlling the operation on employee and project details based on the 
     * choice of user.
     * </p>
     */
    public static void main(String args[]) { 
        int choice;
        EmployeeController employeeController = new EmployeeController();
        ProjectController projectController = new ProjectController();
        ClientController clientContoller = new ClientController();
        do {
            EmployeeView.showMainMenu();
            try {
                choice = Integer.parseInt(UI.getUserInput(Constants.CHOICE));
            } catch(NumberFormatException e) {
                choice = -1;
            }
            switch(choice) {
                case 1:
                    employeeController.showEmployeeCRUD();
                    break;
                case 2:
                    projectController.showProjectCRUD();
                    break;
                case 3:
                    clientContoller.showClientCRUD();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    UI.display(Constants.OPTION_INVALID_MSG);
            }
        } while(choice != 0);
    }

    /**
     * <p>
     * Updating the deatils of the given employee by letting the user to choose
     * what to update and what data is to be updated.
     * </p>
     *
     * @return employee - which contains the updated values given by the user.
     */
    public Employee getUpdationDetailsFromUser(Employee employee) {
        int choice = 0;
        do {
            employeeView.showEmployeeUpdateMenu();
            choice = Integer.parseInt(UI.getUserInput(Constants.CHOICE));
            switch(choice) {
                case 1:
                    employee.setName(UI.getUserInput(Constants.NAME));
                    break;
                case 2:
                    employee.setEmailId(UI.getUserInput(Constants.EMAIL));
                    break;
                case 3:
                    employee.setEmailId(UI.getUserInput(Constants.DOB));
                    break;
                case 5:
                    employee.setAddresses((new AddressView().showAddressCRUD(
                                      (Set<Address>)employee.getAddresses())));
                    break;
                case 0:
                    break;
                default:
                    employeeView.showMessage(Constants.OPTION_INVALID_MSG);
            }
        } while(0 != choice);
        return employee;
    }
}
