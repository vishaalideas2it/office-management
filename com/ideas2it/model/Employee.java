package com.ideas2it.model;

import java.util.Set;

/**
 * <p>
 * Employee entity with getter and setter methods.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class Employee {
    private int id = 0;
    private int age = 0;
    private String name;
    private String emailId ;
    private String dateOfBirth ;
    private Project project;
    private Set addresses;

    public Employee() {}

    public Employee(int id, String name, String emailId,
                       String dateOfBirth, int age) {
        this.id = id;
        this.name = name;
        this.emailId = emailId;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
   
    }

    /**
     * <p>
     * Getter and Setter methods.
     * </p>
     */
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }


    public int getAge() {
        return this.age;  
    }

    public Project getProject() {
        return this.project;
    }

    public Set getAddresses() {
        return this.addresses;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setAddresses(Set addresses) {
        this.addresses = addresses;
    }
}
