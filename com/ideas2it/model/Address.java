package com.ideas2it.model;

/**
 * <p>
 * Employee entity with getter and setter methods.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class Address {
    private int id = 0;
    private String name ;
    private String addressOne ;
    private String addressTwo ;
    private String landMark ;
    private String city ;
    private String zipcode ;
    private String state ;
    private String country ;
    private Client client ;
    private Employee employee ;

    public Address() {}

    public Address(int id, String name, String addressOne,
                       String addressTwo,String city, String zipcode,
                       String state, String country, String landMark) {
        this.id = id;
        this.name = name;
        this.addressOne = addressOne;
        this.addressTwo = addressTwo;
        this.landMark = landMark;
        this.city = city;
        this.zipcode = zipcode;
        this.state = state;
        this.country = country;
    }

    /**
     * <p>
     * Getter and Setter methods.
     * </p>
     */
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getAddressOne() {
        return this.addressOne;
    }

    public String getAddressTwo() {
        return this.addressTwo;
    }

    public String getLandMark() {
        return this.landMark;  
    }

    public String getCity() {
        return this.city;  
    }

    public String getZipcode() {
        return this.zipcode;
    }

    public String getState() {
        return this.state;
    }

    public String getCountry() {
        return this.country;
    }

    public Client getClient() {
        return this.client;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
