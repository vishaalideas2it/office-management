package com.ideas2it.model;

import java.util.Set;

/**
 * <p>
 * Client entity with getter and setter methods.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class Client {
    private int id = 0;
    private String name ;
    private String emailId ;
    private Set projects ;
    private Set addresses ;

    public Client() {}

    public Client(int id) {
        this.id = id;
    }

    public Client(String name, String emailId) {
        this.name = name;
        this.emailId = emailId;
    }

    public String toString() {
        return "[" + id + "-" + name + " " + emailId + "]";
    }

    /**
     * <p>
     * Getter and Setter methods.
     * </p>
     */
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public Set getProjects() {
        return this.projects;
    }

    public Set getAddresses() {
        return this.addresses;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setProjects(Set projects) {
        this.projects = projects;
    }

    public void setAddresses(Set addresses) {
        this.addresses = addresses;
    }
}


