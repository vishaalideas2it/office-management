package com.ideas2it.model;

import java.util.Set;

/**
 * Project entity with getter and setter methods.
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class Project {
    private int id = 0;
    private String name ;
    private Client client;
    private Set employees;

    public Project() {}

    public Project(int id, String name) {
        this.id = id;
        this.name = name;
        this.client = client;
    }

    /**
     * Getter and Setter methods.
     */
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Client getClient() {
        return this.client;
    }

    public Set getEmployees() {
        return this.employees;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setEmployees(Set employees) {
        this.employees = employees;
    }

    public String toString() {
        return "[" + id + " " + name + "]" ;
    }
}


