package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;

/**
 * <p>
 * Implementing a Service for CRUD operation on Address details. The buisness 
 * logics such as adding an address, deleting an address and validating the 
 * information of an address(such as zipcode & holder name) are implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public interface AddressService {

    /** 
     * <p>
     * Adding an address.
     * </p>
     * 
     * @param address - which is to be added in addresses.
     *
     * @return status which indicates the contoller whether address is added or
     *                the details are not in the specified format.
     */
    public void addAddress(Address address, char type) throws UserInputException,
        ApplicationException;

    /**
     *<p>
     * Delete address by given addressdetail
     *</p>
     *@param address - passing an addressDetail
     */
    public void deleteAddress(Address address) throws ApplicationException;

    /** 
     * <p>
     * Reading address by the given holder Id.
     * </p>
     * 
     * @param holderId - which is to be matched with address's holder id.
     * 
     * @return addresses for which the project Id is matching with the given
     *                   project id.
     */
    public Address validateDetailsOfAddress(Address address);
}



