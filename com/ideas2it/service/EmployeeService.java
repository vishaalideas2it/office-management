package com.ideas2it.service;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Employee;

/**
 * <p>
 * Implementing a Service for CRUD operation on employee details. The buisness 
 * logics such as adding an employee to employees, deleting an employee from 
 * employees and validating the information of an employee(such as name & email)
 * are implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public interface EmployeeService {

    /** 
     * <p>
     * Adding an employee to employees.
     * </p>
     * 
     * @param employee - which is to be added in Employees.
     * @param type - to specify the type of operations(insertion or updation)
     */
    public void addEmployee(Employee employee, char type) throws UserInputException,
        ApplicationException;

    /** 
     * <p>
     * Reading an employee from employee's if the given id exists.
     * </p>
     * 
     * @param id - which is to be searched in Employees.
     * 
     * @return employee which is of given id.
     *
     * @exception ApplicationException - if a SQL error occurrs while reading
     *                                   all the employees.
     */
    public Employee getEmployeeById(int id) throws ApplicationException;

    /** 
     * <p>
     * Reading employees from employees, in a project specified by project Id.
     * </p>
     * 
     * @param projectId - which is to be matched with Employee's project id.
     * 
     * @return employees for which the project Id is matching with the given
     *                   project id.
     * @exception ApplicationException - if a SQL error occurrs while reading
     *                                   all the employees.
     */
    public List<Employee> getEmployeesByProjectId(int projectId)
        throws ApplicationException;

    public int getProjectIdByName(String name) throws ApplicationException;

    public Employee validateDetailsOfEmployee(Employee employee);

    /** 
     * <p>
     * Reading all the employees.
     * </p>
     * 
     * @return all the employees.
     *
     */
    public List<String> getAllEmployees() throws ApplicationException;

    /** 
     * <p>
     * Reading all project names.
     * </p>
     * 
     * @return all project names along with the project id.
     *                   
     */
    public List<String> getProjectNames() throws ApplicationException;

    /** 
     * <p>
     * Assigning a given project to the given employee.
     * </p>
     * 
     * @param employeeId - for which the given project is to be assigned.
     * @param projectId - which is given project is to be assigned
     *                   
     */
    public void assignProjectToEmployee(int employeeId, int projectId)
            throws UserInputException;

    /** 
     * <p>
     * UnAssigning project from the given employee.
     * </p>
     * 
     * @param employeeId - for which the project is to be unassigned.
     *                   
     */
    public void unassignProjectFromEmployee(int employeeId)
            throws UserInputException;

    /**
     * <p>
     * Deleting an employee from employees by the given id.
     * </p>
     * 
     * @param id - which specifies the employee to be deleted from employees.
     * 
     * @return true if employee deleted from employees else return false.
     */
    public boolean deleteEmployeeById(int id) throws ApplicationException;

 }



