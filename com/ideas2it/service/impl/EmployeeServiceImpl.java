package com.ideas2it.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.EmployeeDAO;
import com.ideas2it.dao.impl.EmployeeDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.impl.AddressServiceImpl;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.util.Calculation;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on employee details. The buisness 
 * logics such as adding an employee to employees, deleting an employee from 
 * employees and validating the information of an employee(such as name & email)
 * are implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDAO employeeDao = new EmployeeDAOImpl();
    private ProjectService projectService = new ProjectServiceImpl();
    private AddressService addressService = new AddressServiceImpl();
    /**
     * @see com.ideas2it.service.EmployeeService#addEmployee(Employee).
     */
    public void addEmployee(Employee employee, char type) throws
            ApplicationException, UserInputException {
        if (validateDetailsOfEmployee(employee).getId() >= 0) {
            employee.setAge(Calculation.calculateAge(employee.
                                                        getDateOfBirth()));
            employeeDao.updateEmployee(employee, type);
        } else {
            throw new UserInputException(Constants.ERRCD_INVALID, null,
                                            employee);
        }
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeeById(String).
     */
    public Employee getEmployeeById(int id) throws ApplicationException {
		Employee employee = employeeDao.readEmployeeById(id);
        return employee;
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeeById(String).
     */
    public List<String> getProjectNames() throws ApplicationException {
        List<Project> allProjects = projectService.getAllProjects();
        List<String> projectNames = new ArrayList<String>(allProjects.size());
		for(Project project : allProjects)
            projectNames.add(project.getId() + " - " + project.getName());
        return projectNames;
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeesByProjectId(String)
     */
    public List<Employee> getEmployeesByProjectId(int projectId)
        throws ApplicationException {
        return employeeDao.readEmployeesByProjectId(projectId);
    }

    /**
     * @see com.ideas2it.service.EmployeeService#getEmployeesByProjectId(String)
     */
    public List<String> getAllEmployees() throws ApplicationException {
        List<Employee> allEmployees = employeeDao.readAllEmployees();
        List<String> employeeNames = new ArrayList<String>(allEmployees.size());
		for(Employee employee : allEmployees)
            employeeNames.add(employee.getId() + " - " + employee.getName());
        return employeeNames;
    }

    public int getProjectIdByName(String name) throws ApplicationException {
        return projectService.getIdByProjectName(name);
    }

    /**
     * @see com.ideas2it.service.EmployeeService#deleteEmployeeById(String).
     */
    public boolean deleteEmployeeById(int id) throws ApplicationException {
        Employee employee = employeeDao.readEmployeeById(id);
        if (null == employee) {
            employeeDao.updateEmployee(employee, Constants.DELETION);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @see com.ideas2it.service.EmployeeService#assignProjectToEmployee(
                                                                   int, String).
     */
    public void assignProjectToEmployee(int employeeId, int projectId)
            throws UserInputException {
        Employee employee = employeeDao.readEmployeeById(employeeId);
        Project project = projectService.getProjectById(projectId);
        if (null == employee) {
            throw new UserInputException("EMP", null, null);
        } else if (null == project) {
            throw new UserInputException("PROJ", null, null);
        } else {
            employee.setProject(project);
            employeeDao.updateEmployee(employee, Constants.UPDATION);
        }
    }

    /**
     * @see com.ideas2it.service.EmployeeService#unassignProjectFromEmployee
                                                                          (int).
     */
    public void unassignProjectFromEmployee(int employeeId)
            throws UserInputException {
        Employee employee = employeeDao.readEmployeeById(employeeId);
        if (null == employee) {
            throw new UserInputException("EMP", null, null);
        } else {
            employee.setProject(null);
            employeeDao.updateEmployee(employee, Constants.UPDATION);
        }
    }

    /**
     * <p>
     * validating employee's details such as name, email id, salary and DOB.
     * </p>
     *
     * @param employee - which contains the details are to be validated.
     *
     * @return string which states whether validation is ok or which attribute 
     *                is not satisfying the requiered format.
     */
    public Employee validateDetailsOfEmployee(Employee employee) {
        if (!Validation.validateName(employee.getName())) {
            employee.setName(Constants.INVALID);
            employee.setId(-1);
        }
        if (!Validation.validateEmail(employee.getEmailId())) {
            employee.setEmailId(Constants.INVALID);
            employee.setId(-1);
        }
        if (!Validation.checkDateFormat(employee.getDateOfBirth())) {
            employee.setDateOfBirth(Constants.INVALID);
            employee.setId(-1);
        }
        if (!Validation.validateSalary(employee.getSalary())) {
            employee.setSalary(0);
            employee.setId(-1);
        }
        return employee;
    }

 }
