package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.ClientDAO;
import com.ideas2it.dao.impl.ClientDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.service.ClientService;
import com.ideas2it.service.impl.ProjectServiceImpl;
import com.ideas2it.model.Client;
import com.ideas2it.model.Project;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on client details. The buisness 
 * logics such as adding a client to clients, deleting a client from 
 * clients and validating the information of an client(such as name) are 
 * implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class ClientServiceImpl implements ClientService {
    private ClientDAO clientDao = new ClientDAOImpl();

    /**
     * @see com.ideas2it.service.clientService#addclient(client).
     */
    public String addClient(Client client, char type)
             throws ApplicationException, UserInputException {
        if(null == client){
            return " ";
        } else {
            if (validateClientDetails(client)) {
                    clientDao.manipulateClient(client, type);
                    return Constants.STATUS_OK;
            } else {
                throw new UserInputException(Constants.ERRCD_INVALID, null,
                                                client);
            }
        }
    }

    /**
     * @see com.ideas2it.service.clientService#getclient(String).
     */
    public Client getClientById(int id) throws ApplicationException {
		return clientDao.readClientById(id);
    }

    /**
     * @see com.ideas2it.service.clientService#getclient(String).
     */
    public void addProject(Project project) throws ApplicationException,
            UserInputException {
		 new ProjectServiceImpl().addProject(project, 'I');
    }

    /**
     * @see com.ideas2it.service.clientService#getAllclients().
     */
    public List<Client> getAllClients() throws ApplicationException {
       return clientDao.readAllClient();
    }

    /**
     * @see com.ideas2it.service.clientService#deleteclientById(String).
     */
    public boolean deleteClientById(int id) throws ApplicationException {
        Client client = clientDao.readClientById(id);
        if (client == null) {
            return false;
        } else {
            clientDao.manipulateClient(client, Constants.DELETION);
            return true;
        }
    }

    /**
     * @see com.ideas2it.service.clientService#getIdByclientName(String).
     */
    public int getIdByClientName(String name) throws ApplicationException {
        Client client = clientDao.readClientByName(name);
        if (null == client) {
            return 0;
        } else {
            return client.getId();
        }
    }

    /**
     * <p>
     * validating client's details such as name.
     * </p>
     *
     * @param client - which contains the details that has to be
     *                        validated.
     *
     * @return status which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     */
    public boolean validateClientDetails(Client client) {
        boolean valid = true;    
        if (!Validation.validateName(client.getName())) {
            client.setName(Constants.INVALID);
            valid = false;
        }
        if (!Validation.validateEmail(client.getEmailId())) {
            client.setEmailId(Constants.INVALID);
            valid = false;
        }
        return valid;
    }
}



