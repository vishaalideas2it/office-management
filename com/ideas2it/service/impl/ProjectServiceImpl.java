package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.ProjectDAO;
import com.ideas2it.dao.impl.ProjectDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.service.EmployeeService;
import com.ideas2it.service.ProjectService;
import com.ideas2it.service.ClientService;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.model.Employee;
import com.ideas2it.model.Project;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on project details. The buisness 
 * logics such as adding a project to projects, deleting a project from 
 * projects and validating the information of an project(such as name) are 
 * implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class ProjectServiceImpl implements ProjectService {
    private ProjectDAO projectDao = new ProjectDAOImpl();
    private ClientService clientService = new ClientServiceImpl();

    /**
     * @see com.ideas2it.service.ProjectService#addProject(Project).
     */
    public void addProject(Project project, char type)
            throws ApplicationException, UserInputException {
        if (validateProjectDetails(project).getId() >= 0) {
            projectDao.updateProject(project, type);
        } else {
            throw new UserInputException(Constants.ERRCD_INVALID, null, project);
        }
    }

    /**
     * @see com.ideas2it.service.ProjectService#getProject(String).
     */
    public Project getProjectById(int id) throws ApplicationException {
		return projectDao.readProjectById(id);
    }

    /**
     * @see com.ideas2it.service.ProjectService#getAllProjects().
     */
    public List<Project> getAllProjects() throws ApplicationException {
       return projectDao.readAllProject();
    }

    /**
     * @see com.ideas2it.service.ProjectService#getAllEmployees().
     */
    public List<String> getAllEmployees() throws ApplicationException {
        EmployeeService employeeService = new EmployeeServiceImpl();
        return employeeService.getAllEmployees();
    }

    /**
     * @see com.ideas2it.service.ProjectService#deleteProjectById(String).
     */
    public boolean deleteProjectById(int id) throws ApplicationException {
        Project project = projectDao.readProjectById(id);
        if (null == project) {
            return false;
        } else {
            projectDao.updateProject(project, Constants.DELETION);
            return true;
        }        
    }

    /**
     * @see com.ideas2it.service.ProjectService#getIdByProjectName(String).
     */
    public int getIdByProjectName(String name) throws ApplicationException {
        Project project = projectDao.readProjectIdByName(name);
        if (null == project) {
            return 0;
        } else {
            return project.getId();
        }
    }

    /**
     * @see com.ideas2it.service.ProjectService#validateProjectDetails(Project).
     */
    public Project validateProjectDetails(Project project) {  
        if (!Validation.validateName(project.getName())) {
            project.setName(Constants.INVALID);
            project.setId(-1);
        }
        return project;
    }

 }



