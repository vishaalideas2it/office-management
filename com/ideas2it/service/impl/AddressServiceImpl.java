package com.ideas2it.service.impl;

import java.util.List;

import com.ideas2it.common.Constants;
import com.ideas2it.dao.AddressDAO;
import com.ideas2it.dao.impl.AddressDAOImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Address;
import com.ideas2it.model.Client;
import com.ideas2it.service.AddressService;
import com.ideas2it.service.impl.EmployeeServiceImpl;
import com.ideas2it.service.impl.ClientServiceImpl;
import com.ideas2it.util.Validation;

/**
 * <p>
 * Implementing a Service for CRUD operation on Addresss details. The buisness 
 * logics such as adding an address to addresss, deleting an address from 
 * addresss and validating the information of an address(such as name & email)
 * are implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public class AddressServiceImpl implements AddressService {
    private AddressDAO addressDao = new AddressDAOImpl();

    /**
     * @see com.ideas2it.service.AddressService#addAddress(Address).
     */
    public void addAddress(Address address, char type) throws
            ApplicationException, UserInputException {
        if (validateDetailsOfAddress(address).getId() > 0) {
            addressDao.updateAddress(address, type);
        } else {
            throw new UserInputException(Constants.ERRCD_INVALID, null,
                                            address);
        }
    }

    /**
     * @see com.ideas2it.service.AddressService#deleteAddressById(String).
     */
    public void deleteAddress(Address address) throws ApplicationException {
         addressDao.updateAddress(address, Constants.DELETION);
    }

    /**
     * <p>
     * validating address's details such as name, email id, salary and DOB.
     * </p>
     *
     * @param address - which contains the details are to be validated.
     *
     * @return string which states whether validation is ok or which attribute 
     *                is not satisfying the requiered format.
     */
    public Address validateDetailsOfAddress(Address address) {
        if (!Validation.validateName(address.getName())) {
            address.setName(Constants.INVALID);
            address.setId(-1);
        }
        if (!Validation.isValidAddress(address.getAddressOne())) {
            address.setAddressOne(Constants.INVALID);
            address.setId(-1);
        }
        if (!Validation.isValidAddress(address.getAddressTwo())) {
            address.setAddressTwo(Constants.INVALID);
            address.setId(-1);
        }
        if (!Validation.validateName(address.getCity())) {
            address.setCity(Constants.INVALID);
            address.setId(-1);
        }
        if (!Validation.isNumber(address.getZipcode())) {
            address.setZipcode(Constants.INVALID);
            address.setId(-1);
        }
        if (!Validation.validateName(address.getState())) {
            address.setState(Constants.INVALID);
            address.setId(-1);
        }
        if (!Validation.validateName(address.getCountry())) {
            address.setCountry(Constants.INVALID);
            address.setId(-1);
        }
        return address;
    }

 }



