package com.ideas2it.service;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.exception.UserInputException;
import com.ideas2it.model.Project;
import com.ideas2it.model.Employee;
import java.util.List;

/**
 * <p>
 * Implementing a Service for CRUD operation on project details. The buisness 
 * logics such as adding a project to projects, deleting a project from 
 * projects and validating the information of an project(such as name) are 
 * implemented.
 * </p>
 * Auhtor : Vishaal
 * Date   : 16.8.2017
 */
public interface ProjectService {

    /** 
     * <p>
     * Adding a project to projects.
     * </p>
     * 
     * @param project - which is to be added in projects.
     *
     * @return status which indicates the contoller whether project is added or
     *                the details are not in the specified format.
     *
     * @exception ApplicationException - if SQL error occurs, when inserting a
     *            project detail.
     */
    public void addProject(Project project, char type)
        throws ApplicationException, UserInputException;

    /**
     * <p>
     * Reading an project from project's by the given project Id.
     * </p>
     * 
     * @param id - which specifies the project to be searched in projects.
     * 
     * @return project for which project id matching with the given id.
     *
     * @exception ApplicationException - if SQL error occurs, when reading an
     *            project by the given project id.
     */
    public Project getProjectById(int id) throws ApplicationException;



    /**
     * <p>
     * Reading all the Projects.
     * </p>
     * 
     * @return all the existing employees.
     *
     * @exception ApplicationException - if SQL error occurs when reading all the
     *            projects.
     */
    public List<String> getAllEmployees() throws ApplicationException;

    /**
     * <p>
     * Reading all the existing projcts.
     * </p>
     * 
     * @return all the existing projects.
     *
     * @exception ApplicationException - if SQL error occurs when reading all the
     *            projects.
     */
    public List<Project> getAllProjects() throws ApplicationException;

    /**
     * <p>
     * Assigning employee to a project.
     * </p>
     *
     * @param employeeId - which has to be assigned to the given project.
     * @param projectId - which has to be assigned to the given employee.
     *
     * @return status which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     *
     * @exception ApplicationException - if SQL error occurs when deleting an
     *            project by the given project id.
     * @exception UserInputException - if the given projectId or employeeId is
     *            not existing in the record.
     */
    public void assignEmployeesToProject(String employeeId, String projectId)
            throws ApplicationException, UserInputException;

    /**
     * <p>
     * Deleting a project from projects by the given id.
     * </p>
     * 
     * @param id - which specifies the project to be deleted from projects.
     * 
     * @return true if the project specified the given id is deleted else
     *              return false.
     *
     * @exception ApplicationException - if SQL error occurs when deleting an
     *            project by the given project id.
     */
    public boolean deleteProjectById(int id) throws ApplicationException;

    /**
     * <p>
     * Reading a project Id by the given project name from project's.
     * </p>
     * 
     * @param name - which is to be searched in projects.
     * 
     * @return project id for which project name is matching with the given
     *                    name.
     *
     * @exception ApplicationException - if SQL error occurs when reading an
     *            project id by the given project name.
     */
    public int getIdByProjectName(String name) throws ApplicationException;

    /**
     * <p>
     * validating project's details such as name.
     * </p>
     *
     * @param project - which contains the details that has to be
     *                        validated.
     *
     * @return project which states whether validation is ok or which attribute 
     *                is not satisfying the format.
     */
    public Project validateProjectDetails(Project project);

 }



